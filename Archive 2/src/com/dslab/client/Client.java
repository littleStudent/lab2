/**
 * The Client class manages the whole Client process. It starts with the specified arguments and
 * initializes the socket connections. It also handles the console input.
 */

package com.dslab.client;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

import com.dslab.Types.TaskStatusEnum;
import com.dslab.Types.TypeEnum;
import com.dslab.entities.GenericTaskEngineEntity;
import com.dslab.entities.TaskEntity;

public class Client {

	private static ClientModel clientModel;
	private static BufferedReader commandIn = null;
	private static BufferedReader inFromEngine = null;
	private static Socket engineSocket;
	
	protected Client() {
		
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		if (args.length != 3)
	      {
	         System.err.println("Usage: java Client <schedulerHost> <schedulerTCPPort> <taskDir>");
	         System.exit(1);
	      }
		else {
			clientModel = new ClientModel();
			clientModel.setSchedulerHost(args[0]);
			clientModel.setSchedulerTCPPort(Integer.parseInt(args[1]));
			clientModel.setTaskDir(args[2]);
		}
		
		System.out.println("Client started, ready for login ('!login <company> <password>'):");
		initClient();

		
	}

	/**
	 * initialization is done in this method. its called just ones.
	 * Checking the console input in a thread.
	 * --useless-- Listening to the Scheduler for input in a thread.
	 */
	private static void initClient() {
		clientModel.setPreparedTasks(new ArrayList<TaskEntity>());
		checkConsoleInput();
	}
	
	
	/**
	 * checking the console for input all the time in a thread
	 */
	private static void checkConsoleInput() {
		new Thread() {
			public void run() {
				InputStreamReader converter = new InputStreamReader(System.in);
				commandIn = new BufferedReader(converter);
				while(!clientModel.getExit()) {
					final String input;
					try {
						input = commandIn.readLine();
						if(input.equals("!exit"))
							clientModel.setExit(true);
						new Thread() {
							public void run() {
								handleInput(input);
							}
						}.start();
												
					} catch (IOException e) {
						System.out.println(e.getMessage());
						break;
					}
				}
			}
		}.start();
	}
	
	
	
	
	/**
	 * This method is called from the checkConsoleInput(running an a thread)
	 * handleInput decides, depending on @input, what to do.
	 * @param input
	 */
	private static void handleInput(String input) {
		String command = "";
		if( input.lastIndexOf(" ") != -1 ) {
			command = input.substring(0,input.split(" ")[0].length());
		}
		else {
			command = input;
		}
		if(input.lastIndexOf(" ") != -1 && command.equals("!login") && getCharCount(input, " ") == 2 && clientModel.getRegistered() != true) {
			cmdRegisterClient(input);
		} else if(input.lastIndexOf(" ") == -1 && command.equals("!logout") && clientModel.getRegistered() == true) {
			cmdLogoutClient(input);
		} else if(input.lastIndexOf(" ") == -1 && command.equals("!list") && clientModel.getRegistered() == true) {
			cmdListAllTasks();
		} else if(input.lastIndexOf(" ") != -1 && command.equals("!prepare") && getCharCount(input, " ") == 2 && clientModel.getRegistered() == true) {
			cmdPrepareTask(input);
		} else if(input.lastIndexOf(" ") != -1 && command.equals("!requestEngine") && getCharCount(input, " ") == 1 && clientModel.getRegistered() == true) {
			cmdRequestEngineById(input);
		} else if(input.lastIndexOf(" ") != -1 && command.equals("!executeTask") && clientModel.getRegistered() == true) {
			cmdExecuteTask(input);
		} else if(input.lastIndexOf(" ") != -1 && command.equals("!info") && getCharCount(input, " ") == 1 && clientModel.getRegistered() == true) {
			cmdShowTaskInfo(Integer.parseInt(input.substring(input.split(" ")[0].length() + 1)));
		} else if(command.equals("!exit")) {
			cmdExit();
		} else {
			if(clientModel.getRegistered() == true)
				System.out.println("INCORRECT INPUT");
			else
				System.out.println("You have to log in first.");
		}
	}

	/**
	 * command !exit, closes all sockets and exits the Client
	 */
	private static void cmdExit() {
		try {
			if(clientModel.getSchedulerSocket() != null)
				clientModel.getSchedulerSocket().close();
			commandIn.close();
			if(engineSocket != null)
				engineSocket.close();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		
	}

	/**
	 * command !login, registers the client to the scheduler. Checks the username and password, serverside
	 * @param input
	 */
	private static void cmdRegisterClient(String input) {
		try {
			clientModel.setSchedulerSocket(new Socket(clientModel.getSchedulerHost(), 8083));
			clientModel.setPreparedTasks(new ArrayList<TaskEntity>());
			TaskEntity.lastTaskId = 0;
			Runnable task = new ClientTcpSchedulerListenerWorker(clientModel);
			Thread worker = new Thread(task);
			worker.start();
			DataOutputStream outToServer = new DataOutputStream(clientModel.getSchedulerSocket().getOutputStream());
			outToServer.writeBytes(input + "\n");
		} catch (UnknownHostException e) {
			System.out.println("Server not responding.");
			clientModel.setRegistered(false);
		} catch (IOException e) {
			System.out.println(e.getMessage());
			clientModel.setRegistered(false);
		}
	}
	
	/**
	 * logging out the Client. Sending !logout to the Server
	 * @param logout
	 */
	private static void cmdLogoutClient(String input) {
		try {
			DataOutputStream outToServer = new DataOutputStream(clientModel.getSchedulerSocket().getOutputStream());
			outToServer.writeBytes(input + "\n");
		} catch (UnknownHostException e) {
			System.out.println("Server not responding.");
			e.printStackTrace();
			clientModel.setRegistered(true);
		} catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
			clientModel.setRegistered(true);
		}
	}
	
	/**
	 * Command, lists all tasks available in the task directory
	 */
	private static void cmdListAllTasks() {
		for (TaskEntity t : clientModel.getPreparedTasks()) {
			System.out.println(t.getTaskName() + "\n");
		}
	}
	
	/**
	 * Command, shows the info of a specific task
	 * @param taskId
	 */
	private static void cmdShowTaskInfo(int taskId) {
		if(clientModel.getPreparedTasks().size() >= taskId) {
			System.out.println("Task " + taskId + " (" + clientModel.getPreparedTasks().get(taskId - 1).getFileName() + ")");
			System.out.println("Type: " + clientModel.getPreparedTasks().get(taskId - 1).getType());
			System.out.println("Assigned engine: " + clientModel.getPreparedTasks().get(taskId - 1).getAssignedEngine().getIp() + ":" + clientModel.getPreparedTasks().get(taskId - 1).getAssignedEngine().getTcpPort());
			System.out.println("Status: " + clientModel.getPreparedTasks().get(taskId - 1).getStatus());
		}
		else {
			System.out.println("task id not available.");
		}
	}
	
	/**
	 * Command, adds the input task to the prepared Task list
	 * @param input
	 */
	private static void cmdPrepareTask(String input) {
		try {
			File file=new File(clientModel.getTaskDir(), input.split(" ")[1]);
			if(file.exists()) {
				clientModel.getPreparedTasks().add(new TaskEntity(input.split(" ")[1], input.split(" ")[1], TypeEnum.valueOf(input.split(" ")[2]), TaskStatusEnum.prepared)); // TODO: check if the file really exists!!
				System.out.println("Task with id " + clientModel.getPreparedTasks().get(clientModel.getPreparedTasks().size() - 1).getId() + " prepared.");
			}
			else {
				System.out.println("Task not found.");
			}
			
		} catch (IllegalArgumentException e) {
			System.out.println(input.split(" ")[2] + " is not a valid Task type");
		}
		
	}
	
	/**
	 * Command, sends an Engine request to the Server. the request consists of the Task Type
	 * @param input
	 */
	private static void cmdRequestEngineById(String input) {
		try {	
			clientModel.setInput(input);
			DataOutputStream outToServer = new DataOutputStream(clientModel.getSchedulerSocket().getOutputStream());
			if(getTaskTypeById(Integer.parseInt(input.split(" ")[1])) == null)
				System.out.println("No task with Id " + input.split(" ")[1] + "prepared.");
			else if(getTaskById(Integer.parseInt(input.split(" ")[1])).getStatus() == TaskStatusEnum.assigned)
				System.out.println("Assigned engine: " + getTaskById(Integer.parseInt(input.split(" ")[1])).getAssignedEngine().getIp() +
						" Port: " + getTaskById(Integer.parseInt(input.split(" ")[1])).getAssignedEngine().getTcpPort());
			else {
				outToServer.writeBytes("!requestEngine " + getTaskTypeById(Integer.parseInt(input.split(" ")[1])).name() + "\n");
			}
		} catch (UnknownHostException e) {
			System.out.println("Server not responding.");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}
	
	/**
	 * Command, sends the task file to the engine for execution
	 * @param input
	 */
	private static void cmdExecuteTask(String input) {
		engineSocket = null;
		TaskEntity t = getTaskById(Integer.parseInt(input.split(" ")[1]));
		if(t.getAssignedEngine() != null) {
			try {	
				engineSocket = new Socket(t.getAssignedEngine().getIp(), t.getAssignedEngine().getTcpPort());
				
				DataOutputStream outToEngine = new DataOutputStream(engineSocket.getOutputStream());
				inFromEngine = new BufferedReader(new InputStreamReader(engineSocket.getInputStream()));
				
				outToEngine.writeBytes("!executeTask " + t.getFileName() + " " + t.getType() + " \"" + input.split("\"")[1] + "\"" + "\n");
	
				File f = new File(clientModel.getTaskDir(), t.getFileName());
				byte[] mybytearray = new byte[(int) f.length()];
			    BufferedInputStream bis = new BufferedInputStream(new FileInputStream(f));
			    bis.read(mybytearray, 0, mybytearray.length);
			    try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
				}
			    OutputStream os = engineSocket.getOutputStream();
			    System.out.println("Sending...");
			    os.write(mybytearray, 0, mybytearray.length);
			    os.flush();
			    System.out.println("Sent...");
				
				String i = inFromEngine.readLine();
				if(i != null) {
					while(!i.equals("finished transaction") && !clientModel.getExit()) {
						System.out.println("Server: " + i);
						try {
							i = inFromEngine.readLine();
						} catch (IOException e) {
							System.out.println("something went wrong");
							break;
						}
						if(i == null) {
							//System.out.println("something went wrong");
							break;
						}
					}
					//System.out.println("finished task");
				}
			} catch (UnknownHostException e) {
				System.out.println("Server not responding.");
			} catch (IOException e) {
				System.out.println(e.getMessage());
			} 
		} else {
			System.out.println("Task with Id " + t.getId() + " prepared, but no engine assigned.");
		}
	}

	/**
	 * returns the type ot the Taks(id = input)
	 * @param input
	 */
	private static TypeEnum getTaskTypeById(int input) {

		for( TaskEntity t : clientModel.getPreparedTasks()) {
			if(t.getId() == input)
				return t.getType();
		}
		return null;
	}
	
	/**
	 * returns the TaskEntity for a specific id
	 * @param input
	 * @return
	 */
	private static TaskEntity getTaskById(int input) {

		for( TaskEntity t : clientModel.getPreparedTasks()) {
			if(t.getId() == input)
				return t;
		}
		return null;
	}
	
	/**
	* count the characters in a specific string
	* @param text
	* @param ch
	* @return
	*/
	private static int getCharCount(String text, String ch) {
		int count = 0;
		String temp = text;
		while(temp.lastIndexOf(ch)!=-1){
			count++;
			temp = temp.substring(temp.indexOf(ch) + 1);
		}
		return count;
	}

}
