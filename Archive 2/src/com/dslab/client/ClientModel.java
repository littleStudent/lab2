package com.dslab.client;

import java.net.Socket;
import java.util.ArrayList;

import com.dslab.entities.TaskEntity;

public class ClientModel {
	
	private static String input;
	private static String schedulerHost;
	private static int schedulerTCPPort;
	private static String taskDir;
	private static ArrayList<TaskEntity> preparedTasks;
	private static Boolean registered = false;
	private static Boolean exit = false;
	private static Socket schedulerSocket;
	
	protected ClientModel() {
		this.input = "asdfs";
	}
	
	public String getSchedulerHost() {
		return schedulerHost;
	}
	public void setSchedulerHost(String schedulerHost) {
		ClientModel.schedulerHost = schedulerHost;
	}
	public int getSchedulerTCPPort() {
		return schedulerTCPPort;
	}
	public void setSchedulerTCPPort(int schedulerTCPPort) {
		ClientModel.schedulerTCPPort = schedulerTCPPort;
	}
	public String getTaskDir() {
		return taskDir;
	}
	public void setTaskDir(String taskDir) {
		ClientModel.taskDir = taskDir;
	}
	public ArrayList<TaskEntity> getPreparedTasks() {
		return preparedTasks;
	}
	public void setPreparedTasks(ArrayList<TaskEntity> preparedTasks) {
		ClientModel.preparedTasks = preparedTasks;
	}
	public Boolean getRegistered() {
		return registered;
	}
	public void setRegistered(Boolean registered) {
		ClientModel.registered = registered;
	}
	public Socket getSchedulerSocket() {
		return schedulerSocket;
	}
	public void setSchedulerSocket(final Socket schedulerSocket) {
		ClientModel.schedulerSocket = schedulerSocket;
	}
	public String getInput() {
		return input;
	}
	public void setInput(String input) {
		ClientModel.input = input;
	}
	public Boolean getExit() {
		return exit;
	}
	public void setExit(Boolean exit) {
		ClientModel.exit = exit;
	}
}
