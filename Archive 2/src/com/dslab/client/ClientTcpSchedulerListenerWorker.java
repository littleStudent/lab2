/**
 * Listens and reacts to the tcp connection Client - Scheduler
 */

package com.dslab.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

import com.dslab.entities.GenericTaskEngineEntity;
import com.dslab.entities.TaskEntity;

public class ClientTcpSchedulerListenerWorker implements Runnable {
	private ClientModel model;

	ClientTcpSchedulerListenerWorker(ClientModel model) {
		this.model = model;
	}
	
	private TaskEntity getTaskById(int input) {

		for( TaskEntity t : model.getPreparedTasks()) {
			if(t.getId() == input)
				return t;
		}
		return null;
	}

	@Override
	public void run() {
		try {
			BufferedReader inFromServer = new BufferedReader(new InputStreamReader(model.getSchedulerSocket().getInputStream()));
			while(true) {
				String response = inFromServer.readLine();
				if(response != null) {
					if(response.equals("Successfully logged in.")) {
						System.out.println(response);
						model.setRegistered(true);
					} else if(response.equals("login failed")) {
						System.out.println("FROM SERVER: " + response + "\n try again! \n");
						model.setRegistered(false);
					} else if(response.equals("logout complete")) {
						System.out.println("Successfully logged out.");
						model.setRegistered(false);
					} else if(response.equals("finished transaction")) {
						System.out.println("finished task");
					} else if(response.split(" ")[0].equals("!requestEngine")) {
						getTaskById(Integer.parseInt(model.getInput().split(" ")[1])).setAssignedEngine(new GenericTaskEngineEntity(response.split(" ")[1], Integer.parseInt(response.split(" ")[2])));
						System.out.println("Assigned engine: " + getTaskById(Integer.parseInt(model.getInput().split(" ")[1])).getAssignedEngine().getIp() +
								" Port: " + getTaskById(Integer.parseInt(model.getInput().split(" ")[1])).getAssignedEngine().getTcpPort());
					}
					else {
						System.out.println("Server: " + response);
					}
				}
				else {
					System.out.println("logged out from server.");
					model.setRegistered(false);
					break;
				}
			}
		} catch (IOException e) {
			//Thread.
			System.out.println("logged out from server.");
		}
	}
}