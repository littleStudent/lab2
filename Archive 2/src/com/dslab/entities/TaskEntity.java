package com.dslab.entities;

import com.dslab.Types.TaskStatusEnum;
import com.dslab.Types.TypeEnum;

public class TaskEntity {
	
	private int id;
	private String taskName;
	private String fileName;
	private TypeEnum type;
	private TaskStatusEnum status;
	private GenericTaskEngineEntity assignedEngine;
	public static int lastTaskId = 0;
	
	public TaskEntity (final String taskName, final String fileName, final TypeEnum type, final TaskStatusEnum status) {
		this.taskName = taskName;
		this.fileName = fileName;
		this.type = type;
		this.status = status;
		lastTaskId ++;
		this.id = lastTaskId;
		this.assignedEngine = new GenericTaskEngineEntity();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public TypeEnum getType() {
		return type;
	}

	public void setType(TypeEnum type) {
		this.type = type;
	}

	public TaskStatusEnum getStatus() {
		return status;
	}

	public void setStatus(TaskStatusEnum status) {
		this.status = status;
	}

	public GenericTaskEngineEntity getAssignedEngine() {
		return assignedEngine;
	}

	public void setAssignedEngine(GenericTaskEngineEntity assignedEngine) {
		this.assignedEngine = assignedEngine;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

}
