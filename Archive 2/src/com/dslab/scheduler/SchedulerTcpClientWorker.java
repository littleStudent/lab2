/**
 * Handles the tcp connection to the Clients.
 */

package com.dslab.scheduler;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

import com.dslab.Types.*;
import com.dslab.entities.ClientEntity;
import com.dslab.entities.GenericTaskEngineEntity;

public class SchedulerTcpClientWorker implements Runnable {

	private SchedulerModel model;
	private Socket tcpSocket;
	private String companyName = null;
	
	public SchedulerTcpClientWorker(SchedulerModel model, Socket tcpSocket) {
		this.model = model;
		this.tcpSocket = tcpSocket;
	}

	@Override
	public void run() {
		try {
			SchedulerHelper helper = new SchedulerHelper();
			BufferedReader inFromClient = new BufferedReader(new InputStreamReader(tcpSocket.getInputStream()));
			DataOutputStream outToClient = new DataOutputStream(tcpSocket.getOutputStream());
			while(true) {
				String input = inFromClient.readLine();
				if(input != null) {
					if(input.split(" ")[0].equals("!login")) {
						if(input.split(" ").length == 3 && helper.checkLogin(model.getCompanies(), input.split(" ")[1], input.split(" ")[2]) == true) {
							if(model.getClientForCompany(input.split(" ")[1]) != null && 	model.getClientForCompany(input.split(" ")[1]).getActive()) {
								outToClient.writeBytes("Another Client already logged in with the same User\n");
							} else {
								helper.setCompanyStatusforName(model.getClients(), input.split(" ")[1], true, tcpSocket.getPort());
								companyName = input.split(" ")[1];
								outToClient.writeBytes("Successfully logged in.\n");
							}
						}
						else {
							helper.setCompanyStatusforName(model.getClients(), input.split(" ")[1], false, tcpSocket.getPort());
							outToClient.writeBytes("login failed\n");
						}
					} else if (input.split(" ")[0].equals("!logout")) {
						helper.setCompanyStatusforName(model.getClients(), companyName, false, tcpSocket.getPort());
						outToClient.writeBytes("logout complete\n");
					} else if (input.split(" ")[0].equals("!requestEngine")) {
						if(SchedulerHelper.getActiveEngines(model.getEngines()).size() > 0) {
							GenericTaskEngineEntity bestEngine = SchedulerHelper.getMostEfficientEngine(SchedulerHelper.getActiveEngines(model.getEngines()), TypeEnum.valueOf(input.split(" ")[1]));
							if(TypeEnum.valueOf(input.split(" ")[1]) == TypeEnum.LOW) {
									model.getClientForPort(tcpSocket.getPort()).setLowCount(model.getClientForPort(tcpSocket.getPort()).getLowCount() + 1);
							} else if(TypeEnum.valueOf(input.split(" ")[1]) == TypeEnum.MIDDLE) {
									model.getClientForPort(tcpSocket.getPort()).setMiddleCount(model.getClientForPort(tcpSocket.getPort()).getMiddleCount() + 1);
							} else if(TypeEnum.valueOf(input.split(" ")[1]) == TypeEnum.HIGH) {		
									model.getClientForPort(tcpSocket.getPort()).setHighCount(model.getClientForPort(tcpSocket.getPort()).getHighCount() + 1);
							}
							outToClient.writeBytes("!requestEngine " + bestEngine.getIp() + " " + bestEngine.getTcpPort() + "\n");
						} else {
							outToClient.writeBytes("no engine available" + "\n");
						}
					}
				} else {
					System.out.println("client " + model.getClientForPort(tcpSocket.getPort()).getName() + " logged out.");
					model.getClientForPort(tcpSocket.getPort()).setActive(false);
					break;
				}
			}
		} catch (IOException e) {
			
		}
	}

}
