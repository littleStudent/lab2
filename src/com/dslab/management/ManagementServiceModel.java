package com.dslab.management;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;

import com.dslab.entities.ClientEntity;
import com.dslab.entities.CompanyEntity;
import com.dslab.entities.TaskEntity;

public class ManagementServiceModel {

	private static java.util.Properties companiesProperty;
	private ArrayList<CompanyEntity> companies;
	private ArrayList<TaskEntity> tasks;
	private boolean exit;
	private static String bindingName;
	private static String schedulerHost;
	private static int schedulerTCPPort;
	private static int preparationCosts;
	private static String taskDir;
	private Socket engineSocket;
	private Socket schedulerSocket;
	private static TaskEntity currentRequestedTask;
	private HashMap<Integer, Double> priceStepsMap = new HashMap<Integer, Double>();
	private ArrayList<Integer> priceSteps = new ArrayList<Integer>();
	
	protected ManagementServiceModel() {	
		setTasks(new ArrayList<TaskEntity>());
	}
	
	public static java.util.Properties getCompaniesProperty() {
		return companiesProperty;
	}
	public static void setCompaniesProperty(java.util.Properties companiesProperty) {
		ManagementServiceModel.companiesProperty = companiesProperty;
	}
	public ArrayList<CompanyEntity> getCompanies() {
		return companies;
	}
	public void setCompanies(ArrayList<CompanyEntity> companies) {
		this.companies = companies;
	}
	
	/**
	 * refreshes all companies
	 */
	public void refreshCompanies() {
		setCompanies(new ArrayList<CompanyEntity>());
		java.util.Set<String> companyNames = companiesProperty.stringPropertyNames(); // get all company names
		for (String companyName : companyNames) {
			if(companyName.contains(".")) {
				if(getCompanyForName(companyName.split("\\.")[0]) != null) {
					if (companyName.split("\\.")[1].equals("admin")) {
						if (companiesProperty.getProperty(companyName).equals("true")) {
							getCompanyForName(companyName.split("\\.")[0]).setAdmin(true);
						} else {
							getCompanyForName(companyName.split("\\.")[0]).setAdmin(false);
						}
					} else {
						getCompanyForName(companyName.split("\\.")[0]).setCredits(Integer.parseInt(companiesProperty.getProperty(companyName)));
					}
				} else {
					companies.add(new CompanyEntity(companyName.split("\\.")[0], ""));
					if (companyName.split("\\.")[1].equals("admin")) {
						if (companiesProperty.getProperty(companyName).equals("true")) {
							getCompanyForName(companyName.split("\\.")[0]).setAdmin(true);
						} else {
							getCompanyForName(companyName.split("\\.")[0]).setAdmin(false);
						}
					} else {
						getCompanyForName(companyName.split("\\.")[0]).setCredits(Integer.parseInt(companiesProperty.getProperty(companyName)));
					}
				}
			} else {
				if(getCompanyForName(companyName) == null) {
					companies.add(new CompanyEntity(companyName, companiesProperty.getProperty(companyName)));
				} else {
					getCompanyForName(companyName).setPassword(companiesProperty.getProperty(companyName));
				}
			}
		}
	}
	
	private CompanyEntity getCompanyForName(String name) {
		for (CompanyEntity company : companies) {
			if(company.getName().equals(name)) {
				return company;
			}
		}
		return null;
	}
	public boolean isExit() {
		return exit;
	}
	public void setExit(boolean exit) {
		this.exit = exit;
	}
	public static String getBindingName() {
		return bindingName;
	}
	public static void setBindingName(String bindingName) {
		ManagementServiceModel.bindingName = bindingName;
	}
	public static String getSchedulerHost() {
		return schedulerHost;
	}
	public static void setSchedulerHost(String schedulerHost) {
		ManagementServiceModel.schedulerHost = schedulerHost;
	}
	public static int getSchedulerTCPPort() {
		return schedulerTCPPort;
	}
	public static void setSchedulerTCPPort(int schedulerTCPPort) {
		ManagementServiceModel.schedulerTCPPort = schedulerTCPPort;
	}
	public String getTaskDir() {
		return taskDir;
	}
	public static void setTaskDir(String taskDir) {
		ManagementServiceModel.taskDir = taskDir;
	}
	public static int getPreparationCosts() {
		return preparationCosts;
	}
	public static void setPreparationCosts(int preparationCosts) {
		ManagementServiceModel.preparationCosts = preparationCosts;
	}
	public ArrayList<TaskEntity> getTasks() {
		return tasks;
	}
	public void setTasks(ArrayList<TaskEntity> tasks) {
		this.tasks = tasks;
	}

	public Socket getEngineSocket() {
		return engineSocket;
	}

	public void setEngineSocket(Socket engineSocket) {
		this.engineSocket = engineSocket;
	}

	public Socket getSchedulerSocket() {
		return schedulerSocket;
	}

	public void setSchedulerSocket(Socket schedulerSocket) {
		this.schedulerSocket = schedulerSocket;
	}

	public static TaskEntity getCurrentRequestedTask() {
		return currentRequestedTask;
	}

	public static void setCurrentRequestedTask(TaskEntity currentRequestedTask) {
		ManagementServiceModel.currentRequestedTask = currentRequestedTask;
	}

	public HashMap<Integer, Double> getPriceStepsMap() {
		return priceStepsMap;
	}

	public void setPriceStepsMap(HashMap<Integer, Double> priceStepsMap) {
		this.priceStepsMap = priceStepsMap;
	}

	public ArrayList<Integer> getPriceSteps() {
		return priceSteps;
	}

	public void setPriceSteps(ArrayList<Integer> priceSteps) {
		this.priceSteps = priceSteps;
	}
	
}
